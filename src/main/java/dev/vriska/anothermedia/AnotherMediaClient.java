package dev.vriska.anothermedia;

import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer;
import org.quiltmc.qsl.block.extensions.api.client.BlockRenderLayerMap;

import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.minecraft.client.render.RenderLayer;
import vazkii.botania.client.render.tile.RenderTileSpecialFlower;

public class AnotherMediaClient implements ClientModInitializer {
	@Override
	public void onInitializeClient(ModContainer mod) {
		BlockRenderLayerMap.put(RenderLayer.getCutout(), AnotherMedia.crystalanthemum, AnotherMedia.crystalanthemumFloating);

		BlockEntityRendererRegistry.register(AnotherMedia.CRYSTALANTHEMUM, RenderTileSpecialFlower::new);
	}
}
