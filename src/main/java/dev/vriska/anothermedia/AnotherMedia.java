package dev.vriska.anothermedia;

import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.quiltmc.qsl.block.entity.api.QuiltBlockEntityTypeBuilder;
import org.quiltmc.qsl.item.setting.api.QuiltItemSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import vazkii.botania.common.block.BlockFloatingSpecialFlower;
import vazkii.botania.common.block.BlockSpecialFlower;
import vazkii.botania.common.block.ModBlocks;
import vazkii.botania.common.item.block.ItemBlockSpecialFlower;

public class AnotherMedia implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod name as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LoggerFactory.getLogger("Another Media");

	public static final Item CRYSTALLIZED_MANA = new CrystallizedManaItem(new QuiltItemSettings().group(ItemGroup.MISC));

	private static final AbstractBlock.Settings FLOWER_SETTINGS = AbstractBlock.Settings.copy(Blocks.POPPY);
	private static final AbstractBlock.Settings FLOATING_SETTINGS = ModBlocks.FLOATING_PROPS;

	public static final Block crystalanthemum = new BlockSpecialFlower(StatusEffects.SLOWNESS, 30, FLOWER_SETTINGS, () -> AnotherMedia.CRYSTALANTHEMUM);
	public static final Block crystalanthemumFloating = new BlockFloatingSpecialFlower(FLOATING_SETTINGS, () -> AnotherMedia.CRYSTALANTHEMUM);

	public static final BlockEntityType<SubTileCrystalanthemum> CRYSTALANTHEMUM = QuiltBlockEntityTypeBuilder.create(SubTileCrystalanthemum::new, crystalanthemum, crystalanthemumFloating).build(null);

	@Override
	public void onInitialize(ModContainer mod) {
		Registry.register(Registry.ITEM, new Identifier("anothermedia", "crystallized_mana"), CRYSTALLIZED_MANA);

		Registry.register(Registry.BLOCK, new Identifier("anothermedia", "crystalanthemum"), crystalanthemum);
		Registry.register(Registry.ITEM, new Identifier("anothermedia", "crystalanthemum"), new ItemBlockSpecialFlower(crystalanthemum, new QuiltItemSettings().group(ItemGroup.MISC)));
		Registry.register(Registry.BLOCK, new Identifier("anothermedia", "floating_crystalanthemum"), crystalanthemumFloating);
		Registry.register(Registry.ITEM, new Identifier("anothermedia", "floating_crystalanthemum"), new ItemBlockSpecialFlower(crystalanthemumFloating, new QuiltItemSettings().group(ItemGroup.MISC)));

		Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier("anothermedia", "crystalanthemum"), CRYSTALANTHEMUM);
	}
}
