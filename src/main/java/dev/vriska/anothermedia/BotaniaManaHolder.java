package dev.vriska.anothermedia;

import at.petrak.hexcasting.fabric.cc.CCManaHolder;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Unit;
import vazkii.botania.api.BotaniaFabricCapabilities;
import vazkii.botania.api.mana.IManaItem;

public class BotaniaManaHolder extends CCManaHolder {
	private final IManaItem manaItem;

	public BotaniaManaHolder(ItemStack stack) {
		super(stack);

		manaItem = BotaniaFabricCapabilities.MANA_ITEM.find(stack, Unit.INSTANCE);
	}

	@Override
	public boolean canConstructBattery() {
		return false;
	}

	@Override
	public boolean canProvide() {
		return !manaItem.isNoExport(); // this isnt technically correct but i dont care
	}

	@Override
	public boolean canRecharge() {
		return false;
	}

	@Override
	public int getConsumptionPriority() {
		return 40;
	}

	@Override
	public int getMana() {
		return manaItem.getMana();
	}

	@Override
	public int getMaxMana() {
		return manaItem.getMaxMana();
	}

	@Override
	public void setMana(int mana) {
		manaItem.addMana(mana - manaItem.getMana());
	}
}
