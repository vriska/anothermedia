package dev.vriska.anothermedia;

import at.petrak.hexcasting.fabric.cc.CCManaHolder;
import at.petrak.hexcasting.fabric.cc.HexCardinalComponents;
import dev.onyxstudios.cca.api.v3.item.ItemComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.item.ItemComponentInitializer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Unit;
import vazkii.botania.api.BotaniaFabricCapabilities;
import vazkii.botania.common.item.ModItems;

public class AnotherMediaComponents implements ItemComponentInitializer {
	@Override
	public void registerItemComponentFactories(ItemComponentFactoryRegistry registry) {
		registry.register(
				i -> BotaniaFabricCapabilities.MANA_ITEM.find(new ItemStack(i), Unit.INSTANCE) != null,
				HexCardinalComponents.MANA_HOLDER,
				BotaniaManaHolder::new
			);

		registry.register(ModItems.blackLotus, HexCardinalComponents.MANA_HOLDER, s -> new CCManaHolder.Static(
			() -> 8000, 35, s
		));
		registry.register(ModItems.blackerLotus, HexCardinalComponents.MANA_HOLDER, s -> new CCManaHolder.Static(
			() -> 100000, 10, s
		));

		registry.register(AnotherMedia.CRYSTALLIZED_MANA, HexCardinalComponents.MANA_HOLDER, s -> new CCManaHolder.Static(
			() -> 10000, 30, s
		));
	}
}
