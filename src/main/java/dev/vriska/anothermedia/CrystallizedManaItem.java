package dev.vriska.anothermedia;

import net.minecraft.entity.ItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import vazkii.botania.api.item.IManaDissolvable;
import vazkii.botania.api.mana.IManaPool;
import vazkii.botania.common.handler.ModSounds;
import vazkii.botania.common.helper.EntityHelper;
import vazkii.botania.network.EffectType;
import vazkii.botania.network.clientbound.PacketBotaniaEffect;
import vazkii.botania.xplat.IXplatAbstractions;

public class CrystallizedManaItem extends Item implements IManaDissolvable {
	public CrystallizedManaItem(Settings settings) {
		super(settings);
	}

	// copy pasted from ItemBlackLotus lmao
	@Override
	public void onDissolveTick(IManaPool pool, ItemStack stack, ItemEntity item) {
		if (pool.isFull() || pool.getCurrentMana() == 0) {
			return;
		}

		BlockPos pos = pool.getManaReceiverPos();

		if (!item.world.isClient) {
			pool.receiveMana(10000);
			EntityHelper.shrinkItem(item);
			IXplatAbstractions.INSTANCE.sendToTracking(item, new PacketBotaniaEffect(EffectType.BLACK_LOTUS_DISSOLVE, pos.getX(), pos.getY() + 0.5, pos.getZ()));
		}

		item.playSound(ModSounds.blackLotus, 1F, 0.9F);
	}
}
