package dev.vriska.anothermedia;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import vazkii.botania.api.subtile.RadiusDescriptor;
import vazkii.botania.api.subtile.TileEntityFunctionalFlower;

public class SubTileCrystalanthemum extends TileEntityFunctionalFlower {
	protected SubTileCrystalanthemum(BlockEntityType<?> type, BlockPos pos, BlockState state) {
		super(type, pos, state);
	}

	public SubTileCrystalanthemum(BlockPos pos, BlockState state) {
		this(AnotherMedia.CRYSTALANTHEMUM, pos, state);
	}

	@Override
	public int getMaxMana() {
		return 10000;
	}

	@Override
	public int getColor() {
		return 0x4DE1FF;
	}

	@Override
	public RadiusDescriptor getRadius() {
		return new RadiusDescriptor.Circle(getEffectivePos(), 1);
	}

	@Override
	@SuppressWarnings("resource")
	public void tickFlower() {
		super.tickFlower();

		if (getWorld().isClient || redstoneSignal > 0) {
			return;
		}

		if (ticksExisted % 5 == 0) {
			if (getMana() < getMaxMana()) {
				return;
			}

			ItemStack crystal = new ItemStack(AnotherMedia.CRYSTALLIZED_MANA);
			ItemEntity crystalEntity = new ItemEntity(getWorld(), pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, crystal, 0.0, 0.3, 0.0);
			getWorld().spawnEntity(crystalEntity);
			getWorld().playSound(null, getEffectivePos(), SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.BLOCKS, 1F, 1F);

			addMana(-getMaxMana());

			sync();
		}
	}
}
