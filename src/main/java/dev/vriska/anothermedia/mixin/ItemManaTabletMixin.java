package dev.vriska.anothermedia.mixin;

import org.spongepowered.asm.mixin.Mixin;

import at.petrak.hexcasting.api.item.ManaHolderItem;
import net.minecraft.item.ItemStack;
import vazkii.botania.api.mana.IManaItem;
import vazkii.botania.common.item.ItemManaTablet;

@Mixin(ItemManaTablet.class)
public class ItemManaTabletMixin implements ManaHolderItem {
	@Override
	public int getMana(ItemStack stack) {
        IManaItem holder = new ItemManaTablet.ManaItem(stack);

		return holder.getMana();
	}

	@Override
	public int getMaxMana(ItemStack stack) {
        IManaItem holder = new ItemManaTablet.ManaItem(stack);

		return holder.getMaxMana();
	}

	@Override
	public void setMana(ItemStack stack, int mana) {
        IManaItem holder = new ItemManaTablet.ManaItem(stack);

        holder.addMana(mana - holder.getMana());
	}

	@Override
	public boolean manaProvider(ItemStack stack) {
		return true;
	}

	@Override
	public boolean canRecharge(ItemStack stack) {
		return true;
	}
}
